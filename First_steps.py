import pygame

pygame.init()

win = pygame.display.set_mode((500, 540))
pygame.display.set_caption("Pierwsza gra")

KOLOR = (255, 0, 0)
x = 10
y = 30
krok = 20
width = 10
height = 10

run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    pygame.time.delay(50)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        x -= krok
    if keys[pygame.K_RIGHT]:
        x += krok
    if keys[pygame.K_UP]:
        y -= krok
    if keys[pygame.K_DOWN]:
        y += krok


    # pygame.draw.rect(win, "red", (x, y, width, height))
    # pygame.draw.circle(win, "blue", (60, 200), 50, 0)
    # pygame.draw.line(win, "grey", (210, 275), (210,375), 5)
    #
    # font = pygame.font.SysFont("comicsans", 30)
    # label = font.render("Ale ze mnie Pablo Picasso ", 1, (255, 255, 255))
    # win.blit(label, (100, 425))
    win.fill((0, 0, 0))
    pygame.draw.rect(win, "red", (x, y, width, height))
    spaceship = pygame.image.load("spaceship icon.png")
    background = pygame.image.load("starry-night-sky.png")
    win.blit(background, (0, 0))
    win.blit(spaceship, (x, y))
    pygame.display.update()
